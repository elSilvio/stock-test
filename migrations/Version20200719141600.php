<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200719141600 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            'CREATE TABLE IF NOT EXISTS securities 
                (
                    id INT AUTO_INCREMENT NOT NULL, 
                    symbol VARCHAR(3) NOT NULL, 
                    PRIMARY KEY(id)
                )
                DEFAULT CHARACTER SET utf8mb4 
                COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );

        $this->addSql(
            'ALTER TABLE `securities` ADD INDEX( `symbol`)'
        );

        $this->addSql(
        "INSERT INTO securities 
              (id, symbol)
            VALUES
                ('1', 'ABC'),
                ('2', 'BCD'),
                ('3', 'CDE'),
                ('4', 'DEF'),
                ('5', 'EFG'),
                ('6', 'FGH'),
                ('7', 'GHI'),
                ('8', 'HIJ'),
                ('9', 'IJK'),
                ('10', 'JKL')"
        );
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE securities');
    }
}
