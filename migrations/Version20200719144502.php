<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200719144502 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('
          CREATE TABLE IF NOT EXISTS attributes 
          (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, PRIMARY KEY(id)) 
          DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );

        $this->addSql('ALTER TABLE `attributes` ADD INDEX( `name`)');

        $this->addSql(
        "INSERT INTO attributes 
          (id, name)
          VALUES
            (1	, 'price'),
            (2	, 'eps'),
            (3	, 'dps'),
            (4	, 'sales'),
            (5	, 'ebitda'),
            (6	, 'free_cash_flow'),
            (7	, 'assets'),
            (8	, 'liabilities'),
            (9	, 'debt'),
            (10	, 'shares')
        ");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE attributes');
    }
}
