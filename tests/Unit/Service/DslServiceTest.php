<?php

namespace App\Tests\Unit\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Service\DSLService;
use App\Entity\Security;
use App\ValueObject\DSL;
use App\ValueObject\Expression;
use App\Repository\SecuritiesRepository;
use App\Entity\Fact;
use App\Entity\Attribute;

class DslServiceTest extends WebTestCase
{
    /** @var SecuritiesRepository */
    protected $securitiesRepositoryMock;

    public function setUp()
    {
        $this->securitiesRepositoryMock = $this->getMockBuilder('App\Repository\SecuritiesRepository')
            ->disableOriginalConstructor()
            ->setMethods(['findOneBySymbol'])
            ->getMock();
    }

    public function testCreateDSLFromArray()
    {
        $security = new Security();
        $security->setSymbol("ABC");
        $security->setId(1);

        $this->securitiesRepositoryMock
            ->expects($this->once())
            ->method('findOneBySymbol')
            ->will($this->returnValue($security));

        $dslService = new DSLService($this->securitiesRepositoryMock);

        $exampleRequest = json_decode(
            '
                {
                    "expression": {"fn": "*", "a": "sales", "b": 2},
                    "security": "ABC"
                }
            ', true
        );

        $dsl = $dslService->createDSLFromArray($exampleRequest);

        $this->assertInstanceOf(DSL::class, $dsl);
        $this->assertInstanceOf(Expression::class, $dsl->getExpression());
        $this->assertEquals($exampleRequest['expression']['fn'], $dsl->getExpression()->getFn());
        $this->assertEquals($exampleRequest['expression']['a'], $dsl->getExpression()->getA());
        $this->assertEquals($exampleRequest['expression']['b'], $dsl->getExpression()->getB());
    }

    public function testGetValue()
    {
        $attribute = new Attribute();
        $attribute->setName("sales");

        $salesFact = new Fact();
        $salesFact->setAttribute($attribute);
        $salesFact->setValue(4);

        $security = new Security();
        $security->setSymbol("ABC");
        $security->setId(1);
        $security->setFacts([$salesFact]);

        $this->securitiesRepositoryMock
            ->expects($this->once())
            ->method('findOneBySymbol')
            ->will($this->returnValue($security));

        $dslService = new DSLService($this->securitiesRepositoryMock);

        $exampleRequest = json_decode(
            '
                {
                    "expression": {"fn": "*", "a": "sales", "b": 2},
                    "security": "ABC"
                }
            ', true
        );

        $dsl = $dslService->createDSLFromArray($exampleRequest);

        $this->assertEquals(8, $dslService->getValue($dsl));
    }
}
