<?php

namespace App\ValueObject;

class Expression
{
    /** @var string */
    private $fn;

    /** @var string|integer|Expression */
    private $a;

    /** @var string|integer|Expression */
    private $b;

    /**
     * Expression constructor.
     * @param string $fn
     * @param string|array $a
     * @param string|array $b
     */
    public function __construct(string $fn, $a, $b)
    {
        $this->setFn($fn);
        $this->setA($a);
        $this->setB($b);
    }

    public static function fromArray(array $values)
    {
        $a = is_array($values['a']) ? static::fromArray($values['a']) : $values['a'];

        $b = is_array($values['b']) ? static::fromArray($values['b']) : $values['b'];

        return new static($values['fn'], $a, $b);
    }

    /**
     * @return string
     */
    public function getFn(): string
    {
        return $this->fn;
    }

    /**
     * @param string $fn
     */
    public function setFn(string $fn): void
    {
        $this->fn = $fn;
    }

    /**
     * @return Expression|integer|string
     */
    public function getA()
    {
        return $this->a;
    }

    /**
     * @param Expression|integer|string $a
     */
    public function setA($a): void
    {
        $this->a = $a;
    }

    /**
     * @return Expression|integer|string
     */
    public function getB()
    {
        return $this->b;
    }

    /**
     * @param Expression|integer|string $b
     */
    public function setB($b): void
    {
        $this->b = $b;
    }
}
