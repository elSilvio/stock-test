<?php

namespace App\ValueObject;

use App\Entity\Security;

class DSL
{
    /** @var Expression */
    private $expression;

    /** @var Security */
    private $security;

    public function __construct(Expression $expression, Security $security)
    {
        $this->setExpression($expression);
        $this->setSecurity($security);
    }

    /**
     * @return Expression
     */
    public function getExpression(): Expression
    {
        return $this->expression;
    }

    /**
     * @param Expression $expression
     */
    public function setExpression(Expression $expression): void
    {
        $this->expression = $expression;
    }

    /**
     * @return Security
     */
    public function getSecurity(): Security
    {
        return $this->security;
    }

    /**
     * @param Security $security
     */
    public function setSecurity(Security $security): void
    {
        $this->security = $security;
    }
}
