<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Validator\DSLValidator;
use App\Service\DSLService;

/**
 * @Route("/dsl")
 */
class DslController extends AbstractFOSRestController
{
    /** @var DSLService */
    protected $dslService;

    public function __construct(DSLService $dslService)
    {
        $this->dslService = $dslService;
    }

    /**
     * @Route("/evaluate", methods="POST")
     *
     * @param Request $request
     * @param DSLValidator $validator
     *
     * @return array
     */
    public function testAction(Request $request, DSLValidator $validator)
    {
        try {
            $request = $validator->isValid($request);

            $dsl = $this->dslService->createDSLFromArray($request);

            return ['value' => $this->dslService->getValue($dsl)];
        } catch (\Exception $exception) {
            return new JsonResponse(
                ['error' => $exception->getMessage()],
                500
            );
        }
    }
}
