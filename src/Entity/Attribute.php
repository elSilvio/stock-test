<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="attributes")
 * @ORM\Entity(repositoryClass="App\Repository\AttributesRepository")
 */
class Attribute
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Fact", mappedBy="attribut")
     * @ORM\JoinColumn(name="attribute_id", referencedColumnName="id")
     */
    private $facts;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getFacts()
    {
        return $this->facts;
    }

    /**
     * @param mixed $facts
     */
    public function setFacts($facts): void
    {
        $this->facts = $facts;
    }
}
