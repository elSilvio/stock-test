<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="facts")
 * @ORM\Entity(repositoryClass="App\Repository\FactsRepository")
 */
class Fact
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Security", inversedBy="facts")
     */
    private $security;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Attribute", inversedBy="facts")
     */
    private $attribute;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $security_id;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $attribute_id;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $value;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return Attribute
     */
    public function getAttribute() : Attribute
    {
        return $this->attribute;
    }

    /**
     * @param mixed $security
     */
    public function setSecurity($security): void
    {
        $this->security = $security;
    }

    /**
     * @param mixed $attribute
     */
    public function setAttribute($attribute): void
    {
        $this->attribute = $attribute;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }
}
