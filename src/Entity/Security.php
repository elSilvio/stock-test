<?php

namespace App\Entity;

use App\Entity\Fact;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="securities")
 * @ORM\Entity(repositoryClass="App\Repository\SecuritiesRepository")
 */
class Security
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $symbol;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Fact", mappedBy="security")
     * @ORM\JoinColumn(name="security_id", referencedColumnName="id")
     */
    private $facts;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @param mixed $symbol
     */
    public function setSymbol($symbol): void
    {
        $this->symbol = $symbol;
    }

    /**
     * @return Fact[]
     */
    public function getFacts()
    {
        return $this->facts;
    }

    /**
     * @param mixed $facts
     */
    public function setFacts($facts): void
    {
        $this->facts = $facts;
    }
}
