<?php

namespace App\Validator;

use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Symfony\Component\HttpFoundation\Request;

class DSLValidator
{
    public function isValid(Request $request) : array
    {
        $request = json_decode($request->getContent(), true);

        $validator = Validation::createValidator();

        $constraint = new Collection(array(
            'expression'  => new NotBlank(),
            'security'  => new NotBlank(),
        ));

        $violations = $validator->validate($request, $constraint);

        $errors = [];
        foreach ($violations as $violation) {
            $field = preg_replace('/\[|\]/', "", $violation->getPropertyPath());
            $error = $violation->getMessage();
            $errors[] = "$field: " . $error;
        }

        if ($errors) {
            throw new InvalidOptionsException(
                "Missing parameter in request. " . implode(" ", $errors),
                []
            );
        }

        return $request;
    }
}
