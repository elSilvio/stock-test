<?php

namespace App\Service;

use App\Entity\Security;
use App\ValueObject\DSL;
use App\ValueObject\Expression;
use App\Repository\SecuritiesRepository;

class DSLService
{
    /** @var SecuritiesRepository */
    protected $securitiesRepository;

    public function __construct(SecuritiesRepository $securitiesRepository)
    {
        $this->securitiesRepository = $securitiesRepository;
    }

    public function createDSLFromArray(array $values) : DSL
    {
        $expression = Expression::fromArray($values['expression']);

        /** @var Security $security */
        $security = $this->securitiesRepository->findOneBySymbol($values['security']);

        return new DSL(
            $expression,
            $security
        );
    }

    public function getValue(DSL $dsl) : float
    {
        $expression = $dsl->getExpression();
        $security = $dsl->getSecurity();

        return $this->evaluate($expression, $security);
    }

    protected function evaluate(Expression $expression, Security $security) : float
    {
        $a = $expression->getA();

        if ($a instanceof Expression) {
            $aValue = $this->evaluate($a, $security);
        } else {
            $aValue = $this->getFactValue($a, $security);
        }

        $b = $expression->getB();

        if ($a instanceof Expression) {
            $bValue = $this->evaluate($b, $security);
        } else {
            $bValue = $this->getFactValue($b, $security);
        }

        return $this->calculate($aValue, $bValue, $expression->getFn());
    }

    /**
     * @param string|int $attribute
     * @param Security $security
     *
     * @return float
     */
    protected function getFactValue($attribute, Security $security) : float
    {
        if (is_string($attribute)) {
            $facts = $security->getFacts();

            foreach ($facts as $fact) {
                if ($attribute == $fact->getAttribute()->getName()) {
                    return $fact->getValue();
                }
            }
        }

        return $attribute;
    }

    protected function calculate(float $aValue, float $bValue, $operator) : float
    {
        switch($operator) {
            case '*':
                return $aValue * $bValue;
            case '/':
                return $aValue / $bValue;
            case '-':
                return $aValue - $bValue;
            case '+':
                return $aValue + $bValue;
        }
    }
}
