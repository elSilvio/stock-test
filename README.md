## Install with Composer

```
    $ composer install
```

## Set database connection details in .env

```
    $ DATABASE_URL=mysql://root:root@127.0.0.1:3306/stockpedia
```

## Run the migrations

```
    $ bin/console doctrine:migrations:migrate
```

## Start the app

```
    $ symfony server:start
```

## Getting with Curl

```
    $ curl -H 'content-type: application/json' -X POST -d '{"expression": {"fn": "*", "a": "sales", "b": 2},"security": "ABC"}' http://127.0.0.1:8000/dsl/evaluate

    $ curl -H 'content-type: application/json' -X POST -d '{"expression": {"fn": "/", "a": "price", "b": "eps"},"security": "BCD"}' http://127.0.0.1:8000/dsl/evaluate

    $ curl -H 'content-type: application/json' -X POST -d '{"expression": {"fn": "-", "a": {"fn": "-", "a": "eps", "b": "shares"}, "b": {"fn": "-", "a": "assets", "b": "liabilities"}},"security": "CDE"}' http://127.0.0.1:8000/dsl/evaluate
```

## Running tests

```
    $ phpunit or ./bin/phpunit
```
